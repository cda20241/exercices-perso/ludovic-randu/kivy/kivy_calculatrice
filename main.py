from kivy.app import App
from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivymd.app import MDApp
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.textfield import MDTextField


class MainApp(MDApp):
    operateurs = ["/", "*", "+", "-"]
    resultat = None
    title = "Calculatrice"
    reinitialise = False
    dernierBouton = None

    def build(self):
        layout = MDBoxLayout(orientation="vertical", spacing=1, padding=2)
        # Création de l'input (qui affichera le résultat)
        self.affichageResultat = MDTextField(halign="right", font_size=70, readonly=True, height=75)
        # Ajout de l'input au layout
        layout.add_widget(self.affichageResultat)
        # Création d'une liste de boutons dans l'ordre à afficher
        boutons = [
            ["7", "8", "9", "/"],
            ["4", "5", "6", "*"],
            ["1", "2", "3", "+"],
            [".", "0", "C", "-"]
        ]
        # Création des boutons de la liste
        for row in boutons:
            boutons_layout = BoxLayout()
            for label in row:
                bouton = Button(
                    text=label, font_size=30, background_color=[0.408, 0.404, 0.404],
                    padding=10,
                    pos_hint={"top_x": 1, "center_y": 0.5},
                )
                bouton.bind(on_press=self.appuiBouton)
                boutons_layout.add_widget(bouton)
            layout.add_widget(boutons_layout)
        # Création de bouton égal
        boutonEgal = Button(
            text="=", font_size=30, background_normal="", color=[0, 0, 0, 1], background_color=[0.851, 0.678, 0.937],

        )
        boutonEgal.bind(on_press=self.calcul)
        layout.add_widget(boutonEgal)

        return layout

    def appuiBouton(self, instance):
        # on récupère l'operation dans le textInput
        current = self.affichageResultat.text
        # On récupère la dernière touche appuyée
        self.dernierBouton = instance.text

        # Si le dernier bouton appuyé est "C", on reinitialise la calculatrice
        if self.dernierBouton == "C":
            self.affichageResultat.text = ""
        # Si on vient de faire un calcul, on reinitialise la calculatrice
        elif self.reinitialise:
            self.affichageResultat.text = self.dernierBouton
            self.reinitialise = False
        # Sinon on effectue le calcul
        else:
            operationAfaire = current + self.dernierBouton
            self.affichageResultat.text = operationAfaire

    def calcul(self, instance):
        # Si le dernier bouton appuyé est un opérateur, on ne fait rien
        if (self.dernierBouton in self.operateurs):
            return
        # Sinon on affichhe le calcul et on reinitialise
        else:
            solution = str(eval(self.affichageResultat.text))
            self.affichageResultat.text = solution
            self.reinitialise = True


if __name__ == "__main__":
    MainApp().run()
